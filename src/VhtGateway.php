<?php

namespace Vht;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Vht\SoapClient\ClientBuilder;
use Vht\SoapClient\Result\GatewayResult;

class VhtGateway
{
    protected $builder = null;

    public function __construct($phone, $log = false)
    {
        $localDateTime = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        $localDateTime->setTimeZone(new \DateTimeZone('Asia/Ho_Chi_Minh'));

        $builder = new ClientBuilder(
            __DIR__.'/../private/wsdl/vht-gateway.xml'
        );

        if ($log) {
            $log = new Logger('gateway');
            $log->pushHandler(
                new StreamHandler(
                    BASE_DIR . 'logs/' . $localDateTime->format('Ymd') . '/' . $phone . '_' . $localDateTime->format('His.u') . '_soap.log'
                )
            );
            $this->builder = $builder->withLog($log)->build();
        } else {
            $this->builder = $builder->build();
        }
    }

    public function receiveMO($serviceId, $userId, $requestId, $message, $username, $commandCode, $password)
    {
        $result = $this->builder->receiveMO($serviceId, $userId, $requestId, $message, $username, $commandCode, $password);
        $resultCode = $result->return;

        $smsResult = new GatewayResult();
        $smsResult->setMessage('');
        $smsResult->setStatusCode($resultCode);

        return $smsResult;
    }
}