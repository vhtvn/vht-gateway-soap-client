<?php
namespace Vht\SoapClient;

use Vht\SoapClient\Result;

/**
 * Gateway API client interface
 *
 */
interface ClientInterface
{
    /**
     * Logs in to the login server and starts a client session
     *
     * @return Result\LoginResult
     * @link
     */
    public function login();
    
    /**
     * @param $serviceId
     * @param $userId
     * @param $requestId
     * @param $message
     * @param $username
     * @param $commandCode
     * @param $password
     * 
     * @return Result\ReceiveMOResult
     * @link http://app.vht.com.vn:8440/vht/services/vht-gateway?wsdl
     */
    public function receiveMO($serviceId, $userId, $requestId, $message, $username, $commandCode, $password);
    
    /**
     * @param $serviceId
     * @param $userId
     * @param $contentType
     * @param $messageType
     * @param $totalMessage
     * @param $messageIndex
     * @param $isMore
     * @param $message
     * @param $messageId
     * @param $username
     * @param $commandCode
     * @param $password
     * @param $operator
     *
     * @return Result\SendMTResult
     * @link http://app.vht.com.vn:8440/vht/services/vht-gateway?wsdl
     */
    public function sendMT($serviceId, $userId, $contentType, $messageType, $totalMessage, $messageIndex, $isMore, $message, $messageId, $username, $commandCode, $password, $operator);


}

