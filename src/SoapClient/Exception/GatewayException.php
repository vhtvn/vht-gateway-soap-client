<?php

namespace Vht\SoapClient\Exception;

use Exception;

class GatewayException extends Exception
{

    protected $message;
    protected $errorCode;

    public function __construct()
    {

    }

    function setGatewayExceptionMessage($message)
    {
        $this->message = $message;
    }

    function getGatewayExceptionMessage()
    {
        return $this->message;
    }

    function setGatewayExceptionErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    function getSmsExceptionErrorCode()
    {
        return $this->errorCode;
    }

}