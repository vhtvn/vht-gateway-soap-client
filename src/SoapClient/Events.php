<?php
namespace Vht\SoapClient;

final class Events
{
    const REQUEST    = 'gateway.soap_client.request';
    const RESPONSE   = 'gateway.soap_client.response';
    const FAULT      = 'gateway.soap_client.fault';
}

