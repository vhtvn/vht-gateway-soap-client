<?php
namespace Vht\SoapClient;

use Vht\SoapClient\Result\LoginResult;
use Vht\Common\AbstractHasDispatcher;
use Vht\SoapClient\Soap\SoapClient;
use Vht\SoapClient\Result;
use Vht\Common\Event;

/**
 * A client for the Gateway SOAP API
 *
 */
class Client extends AbstractHasDispatcher implements ClientInterface
{
    /**
     * SOAP namespace
     *
     * @var string
     */
    const SOAP_NAMESPACE = '';

    /**
     * PHP SOAP client for interacting with the Gateway API
     *
     * @var SoapClient
     */
    protected $soapClient;

    /**
     * Login result
     *
     * @var Result\LoginResult
     */
    protected $loginResult;

    /**
     * Construct Gateway SOAP client
     *
     * @param SoapClient $soapClient SOAP client
     *
     */
    public function __construct(SoapClient $soapClient)
    {
        $this->soapClient = $soapClient;
    }

    public function doLogin()
    {
        $loginResult = new LoginResult();
        $this->setLoginResult($loginResult);

        return $loginResult;
    }

    public function login()
    {
        return $this->doLogin();
    }

    /**
     * Get login result
     *
     * @return Result\LoginResult
     */
    public function getLoginResult()
    {
        if (null === $this->loginResult) {
            $this->login();
        }

        return $this->loginResult;
    }

    protected function setLoginResult(Result\LoginResult $loginResult)
    {
        $this->loginResult = $loginResult;
    }

    /**
     * @param $serviceId
     * @param $userId
     * @param $requestId
     * @param $message
     * @param $username
     * @param $commandCode
     * @param $password
     *
     * @return Result\ReceiveMOResult
     * @throws \SoapFault
     */
    public function receiveMO($serviceId, $userId, $requestId, $message, $username, $commandCode, $password)
    {
        return $this->call(
            'receiveMO',
                array(
                    'serviceId' => $serviceId,
                    'userId' => $userId,
                    'requestId' => $requestId,
                    'message' => $message,
                    'username' => $username,
                    'commandCode' => $commandCode,
                    'password' => $password,
                )
        );
    }

    /**
     * @param $serviceId
     * @param $userId
     * @param $contentType
     * @param $messageType
     * @param $totalMessage
     * @param $messageIndex
     * @param $isMore
     * @param $message
     * @param $messageId
     * @param $username
     * @param $commandCode
     * @param $password
     * @param $operator
     *
     * @return Result\SendMTResult
     * @throws \SoapFault
     */
    public function sendMT($serviceId, $userId, $contentType, $messageType, $totalMessage, $messageIndex, $isMore, $message, $messageId, $username, $commandCode, $password, $operator)
    {
        return $this->call(
            'sendMT',
                array(
                    'serviceId' => $serviceId,
                    'userId' => $userId,
                    'contentType' => $contentType,
                    'messageType' => $messageType,
                    'totalMessage' => $totalMessage,
                    'messageIndex' => $messageType,
                    'isMore' => $isMore,
                    'message' => $message,
                    'messageId' => $messageId,
                    'username' => $username,
                    'commandCode' => $commandCode,
                    'password' => $password,
                    'operator' => $operator,
                )
        );
    }

    /**
     * Initialize connection
     *
     */
    protected function init()
    {
    }

    /**
     * Issue a call to Gateway API
     *
     * @param string $method SOAP operation name
     * @param array  $params SOAP parameters
     *
     * @return array | $result object, such as QueryResult, SaveResult, DeleteResult.
     * @throws \Exception
     * @throws \SoapFault
     */
    protected function call($method, array $params = array())
    {
        $this->init();

        $requestEvent = new Event\RequestEvent($method, $params);
        $this->dispatch(Events::REQUEST, $requestEvent);

        try {
            $result = $this->soapClient->$method($params);
        } catch (\SoapFault $soapFault) {
            $faultEvent = new Event\FaultEvent($soapFault, $requestEvent);
            $this->dispatch(Events::FAULT, $faultEvent);

            throw $soapFault;
        }

        if (!isset($result)) {
            return array();
        }

        $this->dispatch(
            Events::RESPONSE,
            new Event\ResponseEvent($requestEvent, $result)
        );

        return $result;
    }
}